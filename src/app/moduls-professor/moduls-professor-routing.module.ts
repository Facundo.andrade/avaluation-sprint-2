import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModulsProfessorPage } from './moduls-professor.page';

const routes: Routes = [
  {
    path: '',
    component: ModulsProfessorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModulsProfessorPageRoutingModule {}
