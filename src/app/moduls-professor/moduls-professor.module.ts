import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModulsProfessorPageRoutingModule } from './moduls-professor-routing.module';

import { ModulsProfessorPage } from './moduls-professor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModulsProfessorPageRoutingModule
  ],
  declarations: [ModulsProfessorPage]
})
export class ModulsProfessorPageModule {}
