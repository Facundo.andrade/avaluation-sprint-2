import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-moduls-professor',
  templateUrl: './moduls-professor.page.html',
  styleUrls: ['./moduls-professor.page.scss'],
})
export class ModulsProfessorPage implements OnInit {

  user : any;

  moduls:any[];

  constructor(private route: ActivatedRoute, private router: Router, private apiService: ApiserviceService) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.user = this.router.getCurrentNavigation().extras.state.user;
        this.moduls = this.router.getCurrentNavigation().extras.state.modulos;
      }
    });
  
  }

  ngOnInit() {
    this.getModulos(this.user);
  }

  goToTable(modul:any) {
    let navigationExtras: NavigationExtras = {
      state: {
        modulo: modul,
        user: this.user
      }
    };
    this.router.navigate(['notes'], navigationExtras);
  }

  async getModulos(user:any) {
    this.moduls = await this.apiService.getModulos(user);
  }
}
