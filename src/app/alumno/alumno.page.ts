import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.page.html',
  styleUrls: ['./alumno.page.scss'],
})
export class AlumnoPage implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private apiService: ApiserviceService) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.dni = this.router.getCurrentNavigation().extras.state.user.dni;
        this.user = this.router.getCurrentNavigation().extras.state.user;
      }
    });
  }

  user : any;
  tasques:any[];
  modulos:any[];
  dni:any;

  async ngOnInit() {
    this.tasques = await this.apiService.getNotaAlumno({dni: this.dni})
    this.modulos = await this.apiService.getModulosAumno({dni: this.dni})
  }

  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
  };

  goToTable(modul:any){
    let navigationExtras: NavigationExtras = {
      state: {
        modulo: modul,
        user: this.user
      }
    };
    this.router.navigate(['notes-alumno'], navigationExtras);
  }

}