import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'moduls-professor',
    loadChildren: () => import('./moduls-professor/moduls-professor.module').then( m => m.ModulsProfessorPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'notes',
    loadChildren: () => import('./notes/notes.module').then( m => m.NotesPageModule)
  },
  {
    path: 'add-ejercicio',
    loadChildren: () => import('./add-ejercicio/add-ejercicio.module').then( m => m.AddEjercicioPageModule)
  },
  {
    path: 'alumno',
    loadChildren: () => import('./alumno/alumno.module').then( m => m.AlumnoPageModule)
  },
  {
    path: 'notes-alumno',
    loadChildren: () => import('./notes-alumno/notes-alumno.module').then( m => m.NotesAlumnoPageModule)
  },
  {
    path: 'ausencias',
    loadChildren: () => import('./ausencias/ausencias.module').then( m => m.AusenciasPageModule)
  },
  {
    path: 'notes-detail',
    loadChildren: () => import('./notes-detail/notes-detail.module').then( m => m.NotesDetailPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
