import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiserviceService } from '../services/apiservice.service';


@Component({
  selector: 'app-notes-detail',
  templateUrl: './notes-detail.page.html',
  styleUrls: ['./notes-detail.page.scss'],
})
export class NotesDetailPage implements OnInit {
  

  Alumno:any;
  dniAlumno:any;
  nomAlumno:any;
  NotasDetail:any[];

  constructor(private route: ActivatedRoute, private router: Router, private apiService: ApiserviceService) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.Alumno = this.router.getCurrentNavigation().extras.state.parametros;
        this.nomAlumno = this.Alumno.nombre_alumno;
        this.dniAlumno = this.Alumno.dni_alumno;
      }
    });
  }

  ngOnInit() {
    console.log(this.nomAlumno);
    console.log(this.dniAlumno);
    this.getDetailedNotas(this.dniAlumno);
  }

  async getDetailedNotas(dni){
    this.NotasDetail = await this.apiService.getDetailedNotas({dni: dni});
    console.log(this.NotasDetail)
  }

  
  deleteButton(notas){
    console.log(notas);
    this.apiService.deleteNota(notas);
  }

  updateButton(notas){
    console.log(notas)
    this.apiService.updateNotas(notas);
  }

}
