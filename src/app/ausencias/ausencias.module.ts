import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AusenciasPageRoutingModule } from './ausencias-routing.module';

import { AusenciasPage } from './ausencias.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AusenciasPageRoutingModule
  ],
  declarations: [AusenciasPage]
})
export class AusenciasPageModule {}
