import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { ExcelService } from './services/excel-service.service';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  exports: [ExcelService],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, ExcelService],
  bootstrap: [AppComponent],
})
export class AppModule {}
