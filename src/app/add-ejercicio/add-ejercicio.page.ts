import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-add-ejercicio',
  templateUrl: './add-ejercicio.page.html',
  styleUrls: ['./add-ejercicio.page.scss'],
})
export class AddEjercicioPage implements OnInit {

  progress= 0;
  nombre_entrega 

  entrega:any = {
    "nombre_entrega" : " ",
    "fecha_entrega" : " ",
    "dni_alumno" : " ",
    "dni_profesor" : " ",
    "nombre_modulo" : " ",
    "entregada" : 0,
    "uf" : 0,
    "nota" : 0
  }

  dnis:any[];

  constructor(private route: ActivatedRoute, private router: Router, private apiService: ApiserviceService) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.entrega.nombre_modulo = this.router.getCurrentNavigation().extras.state.nomModul;
        this.entrega.dni_profesor = this.router.getCurrentNavigation().extras.state.dniprofe;
        this.dnis = this.router.getCurrentNavigation().extras.state.dnis;
      }
    });
  }

  ngOnInit() {
  }

  checkFocus(){

    this.progress = this.progress + 0.33;

  }

  pasoParametros(nombre, uf, fecha){
    
    console.log(nombre);
    console.log(uf);

    fecha = fecha.substring(
      fecha.lastIndexOf("T"),
      fecha.lastIndexOf(" ")
      
    );

    console.log(fecha)

    console.log(this.dnis[0], this.dnis[1])
    this.entrega.nombre_entrega = nombre;
    this.entrega.uf = uf;
    this.entrega.fecha_entrega = fecha;
    console.log(this.entrega)

    for (var i = 0; i < this.dnis.length; i++){
      this.entrega.dni_alumno = this.dnis[i];
      this.apiService.setNotas(this.entrega);

    }
  }
}